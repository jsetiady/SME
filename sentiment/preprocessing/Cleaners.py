from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import preprocessor
from gensim import utils
import re
from bs4 import BeautifulSoup
import pandas as pd
# from typofix.spell import correction, qwerty_correction

def string_to_unicode(string):
    unicode = utils.to_unicode(string)
    return unicode

def stem(raw_data):
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    stemmed_data = stemmer.stem(raw_data)
    return stemmed_data

def raw_to_text(raw_data):
    if (type(raw_data)==float):
        raw_data = str(raw_data)
    text = BeautifulSoup(raw_data, "html.parser").get_text()
    return text

def remove_urls(text_with_urls):
    preprocessor.set_options(preprocessor.OPT.URL)
    text_without_urls = preprocessor.clean(text_with_urls)
    return text_without_urls

def remove_twitter_mentions(text_with_mentions):
    preprocessor.set_options(preprocessor.OPT.MENTION)
    text_without_mentions = preprocessor.clean(text_with_mentions)
    return text_without_mentions

def keep_letters_only(text_with_more_than_letters):
    text_with_letters_only = re.sub("[^a-zA-Z]", " ",
                                    text_with_more_than_letters)
    return text_with_letters_only

def lowercase_all(text_with_uppercase):
    if (type(text_with_uppercase)==float):
        text_with_uppercase = str(text_with_uppercase)
    text_with_only_lowercase = text_with_uppercase.lower()
    return text_with_only_lowercase

def substitute(sentence, sub_source):
    sub_table= pd.read_csv(sub_source, header=0,
                         delimiter=",", quoting=3)

    sub_dict = {}
    
    for i in xrange(sub_table["Word"].size):
 
        slang = lowercase_all(sub_table["Word"][i])
        subs = lowercase_all(sub_table["Substitution"][i])

        sub_dict[sub_table["Word"][i]] = sub_table["Substitution"][i]

    token = lowercase_all(sentence).split()

    new_sentence = []
    for word in token:
        if word in sub_dict:
            new_sentence.append(sub_dict[word])
        else:
            new_sentence.append(word)

    return " ".join(new_sentence)

def typofix(sentence):
    new_sentence = []

    tokens = sentence.split()
    for word in tokens:
    #    print "The word is " + word
        new_sentence.append(qwerty_correction(word))

    return " ".join(new_sentence)

def clean(sentence):
    cleaned_sentence = raw_to_text(sentence)
    cleaned_sentence = remove_urls(cleaned_sentence)
    cleaned_sentence = remove_twitter_mentions(cleaned_sentence)
    cleaned_sentence = keep_letters_only(cleaned_sentence)
    cleaned_sentence = lowercase_all(cleaned_sentence)
    # cleaned_sentence = substitute(cleaned_sentence,'./datasets/sub_table.csv')
#    print "Start fixing " + cleaned_sentence
#    cleaned_sentence = typofix(cleaned_sentence)
#    print "End fixing " + cleaned_sentence
    cleaned_sentence = string_to_unicode(cleaned_sentence)
    
    return cleaned_sentence

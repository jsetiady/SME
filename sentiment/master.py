from preprocessing.Readers import tsvs_to_df
from preprocessing.Tokenizers import bigram_df, process_df 
from build_models.build_models import build_sentence_vector
from gensim.models import Word2Vec
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from gensim.models.wrappers import FastText
import numpy as np
import pandas as pd

class word_vector_model:
    def __init__(model_name, train_sentences, model_size):
        model_key = {'word2vec': Word2Vec(train_sentences,
                                       size=model_size,
                                       workers=2,
                                       iter=5,
                                       window=5,
                                       sg=1,
                                       hs=1,
                                       min_count=5),
                  'fasttext': FastText(train_sentences, 
                                       size=model_size)}
        return model_key[model_name]


def train_wvm(train_sentences, model='word2vec', model_size=300):
     model_key = {'word2vec': Word2Vec(train_sentences,
                                       size=model_size,
                                       workers=2,
                                       iter=5,
                                       window=5,
                                       sg=1,
                                       hs=1,
                                       min_count=5),
                  'fasttext': FastText(train_sentences, 
                                       size=model_size)}
     return model_key[model]

def vectorize_sentences(sentences, model, model_size=300):
    vectors = np.zeros((len(sentences),model_size))
    for i in xrange(0,len(vectors)):
        vectors[i] = build_sentence_vector(model, sentences, model_size)
    return vectors

def train_clf(train_vectors,train_labels,clf_name='maxent'):
    clf_key = {'maxent' : LogisticRegression(), 
               'forest' : RandomForestClassifier(),
               'MLP' : MLPClassifier()}
    clf = clf_key[clf_name]
    clf.fit(train_vectors,train_labels)
    return clf

def train(model_name='word2vec', model_size=300,
          clf_name='maxent', 
          unsup_train_files=['./datasets/fb_large(14k).tsv'], 
          sup_train_files=['./datasets/facebook_part1.tsv'],
          test_files=['./datasets/facebook_part2.tsv']):
    unsup_df = bigram_df(process_df(tsvs_to_df(unsup_train_files)))
    sup_df = bigram_df(process_df(tsvs_to_df(sup_train_files)))
    test_df = bigram_df(process_df(tsvs_to_df(test_files)))
    
    wvm_train_df = pd.concat([unsup_df,sup_df], ignore_index=True)
    model = train_wvm(wvm_train_df["Text"],model_name,model_size)
    
    train_vectors = vectorize_sentences(sup_df,model)
    test_vectors = vectorize_sentences(test_df,model)

    train_labels = sup_df["Sentiment"].as_matrix()
    test_labels = test_df["Sentiment"].as_matrix()

    clf = train_clf(train_vectors,train_labels,clf_name)

    return model, clf

model, clf = train()
f = open('./logs/master.txt', 'w')

sup_df = bigram_df(process_df(tsvs_to_df(sup_train_files)))
test_df = bigram_df(process_df(tsvs_to_df(test_files)))
train_vectors = vectorize_sentences(sup_df,model)
test_vectors = vectorize_sentences(test_df,model)
train_labels = sup_df["Sentiment"].as_matrix()
test_labels = test_df["Sentiment"].as_matrix()
sup_train_files=['./datasets/facebook_part1.tsv']
test_files=['./datasets/facebook_part2.tsv']

log_all_classifier_tests(f, 
                         'blah', 'blah',
                         train_vectors,train_labels,
                         test_vectors,test_labels)

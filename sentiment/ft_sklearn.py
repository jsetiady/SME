# use gensim 
import gensim
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import scale
from build_models.build_models import load_fasttext
from build_models.build_models import build_word_vector
from preprocessing.Readers import tsv_to_dataframe
from preprocessing.Tokenizers import df_to_tokenized_sentences
from tests import log_all_classifier_tests
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from build_classifiers.Logger import graph_vs_datasize
# import tqdm
"""
dictFileName = './models/fb_1_pretrained'
model = gensim.models.KeyedVectors.load_word2vec_format(
    dictFileName+'.bin', binary=True) 
model.save_word2vec_format(dictFileName+".bin", binary=True) 
model = gensim.models.KeyedVectors.load_word2vec_format(
    dictFileName+".bin", binary=True)

# .bin not compataible w/ gensim 
# model = load_fasttext('./fasttext/fb_1_pretrained.bin')
"""
# the legit one  
model = load_fasttext('./models/wiki.id.vec')
model_size = 300 

train_df = tsv_to_dataframe('./datasets/facebook_part1.tsv')
train_sentences = df_to_tokenized_sentences(train_df)
train_num = train_df.shape[0]
'''
cannot train bc/ keyedvector, not w2v type
model.train(train_sentences, total_examples=1000, epochs=5)
'''
train_array  = np.zeros((train_num,model_size))
train_labels = np.zeros(train_num)

test_df = tsv_to_dataframe('./datasets/facebook_part2.tsv')
test_sentences = df_to_tokenized_sentences(test_df)
test_num = test_df.shape[0]

test_array = np.zeros((test_num,model_size))

tfidfer = TfidfVectorizer(analyzer = "word",   
                             tokenizer = None,
                             preprocessor = None,
                             stop_words = None,
                             max_features = 500)
tfidf_matrix = tfidfer.fit_transform([" ".join(x) for x in train_sentences])

"""
train_array = np.concatenate([build_word_vector(s,model_size) for s
                            in train_sentences])

test_array = np.concatenate([build_word_vector(s,model_size) for s 
                            in test_sentences])
"""
for i in xrange(0,train_num):
    train_array[i] = build_word_vector(model,train_sentences[i],model_size)

for i in xrange(0,test_num):
    test_array[i] = build_word_vector(model,test_sentences[i],model_size)

maxent = LogisticRegression()
forest = RandomForestClassifier()
forest.fit(train_array,train_df["Sentiment"])
graph_vs_datasize('ft_sklearn', forest, 10, 'accuracy',
                    train_array,train_df["Sentiment"],
                    test_array,test_df["Sentiment"])

"""
train_array = scale(train_array)
test_array = scale(test_array)
"""
f = open('./logs/ft_sklearn.txt','w')
log_all_classifier_tests(f,
                        './datasets/facebook_part1.tsv',
                        './datasets/facebook_part2.tsv',
                        train_array,train_df["Sentiment"][0:train_num],
                        test_array,test_df["Sentiment"][0:test_num])
f.close()

# use fasttext python package
# https://pypi.python.org/pypi/fasttext
#import fasttext

from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Tokenizers import process_df, bigram_df
from preprocessing.Readers import tsv_to_dataframe
from keras.models import Sequential
from keras.layers import *
import matplotlib.pyplot as plt
from keras.optimizers import *
import numpy as np
from Attention import *

timesteps = 37
x_train = np.empty([950,timesteps,300])
x_test = np.empty([950,timesteps,300])
y_train = np.empty([950,3])
y_test = np.empty([950,3])

model = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300

train_df = tsv_to_dataframe('./datasets/facebook_part1.tsv')
test_df = tsv_to_dataframe('./datasets/facebook_part2.tsv')

train_sentences = bigram_df(process_df(train_df))
test_sentences = bigram_df(process_df(test_df))

def load():
    
    np.set_printoptions(threshold='nan')

    for i in range(950):
        for j in range(min(len(train_sentences["Text"][i]),timesteps-1)):
            try:
                a = model[train_sentences["Text"][i][j]]
                x_train[i][j] = a
            except KeyError:
                x_train[i][j] = np.zeros((300))
                continue
        for k in range(min(len(train_sentences["Text"][i]),timesteps-1),timesteps):
            x_train[i][k] = np.zeros((300))
        y_train[i] = [0,0,0]
        y_train[i][train_sentences["Sentiment"][i]+1]=1

    for i1 in range(950):
        for j1 in range(min(len(test_sentences["Text"][i1]),timesteps-1)):
            try:
                x_test[i1][j1] = model[test_sentences["Text"][i1][j1]]
            except KeyError:
                x_test[i1][j1] = np.zeros((300))
                continue
        for k1 in range(min(len(test_sentences["Text"][i1]),timesteps-1),timesteps):
            x_test[i1][k1] = np.zeros((300))
        y_test[i1] = [0,0,0]
        y_test[i1][test_sentences["Sentiment"][i1]+1]=1

def create_model():

    dimensions = 300
    batch_size = 80
    epochs = 15
    seq=True

    model = Sequential()
    model.add(Masking(mask_value=0, input_shape=(timesteps, dimensions)))
    model.add(Bidirectional(LSTM( dimensions,  input_shape=(timesteps, dimensions), return_sequences=seq)))
    model.add(Bidirectional(LSTM( dimensions,  input_shape=(timesteps, dimensions), return_sequences=True)))
    model.add(Attention())
    model.add(Dropout(0.5))
    #model.add(Dense(3, activation='sigmoid'))
    model.add(Dense(3, activation='softmax'))
    model.compile(loss="categorical_crossentropy", 
                  optimizer=Adagrad(lr=0.01, clipvalue=0.1, epsilon=1e-9), 
                  metrics=['accuracy'])
    print(model.summary())
    #estimator = KerasClassifier(build_fn=baseline_model, epochs=epochs, batch_size=5, verbose=0)
    h = model.fit(x_train, y_train, epochs=epochs,  batch_size=batch_size,
              verbose = 1, shuffle=True, validation_data=(x_test,y_test))
    
    np.set_printoptions(threshold='nan')

    print np.hstack((model.predict(x_test),y_test))
   # print y_test
    print(h.history.keys())

    plt.plot(h.history['acc'])
    plt.plot(h.history['val_acc'])
    plt.plot(h.history['loss'])
    plt.plot(h.history['val_loss'])
    #plt.plot(h.history['acc']-h.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

    #plt.plot(h.history['loss'])
    #plt.plot(h.history['val_loss'])
    #plt.title('model loss')
    #plt.ylabel('loss')
    #plt.xlabel('epoch')
    #plt.legend(['train', 'test'], loc='upper left')
    #plt.show()

load()
create_model()






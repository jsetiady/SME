from preprocessing.Readers import tsv_to_dataframe
from preprocessing.Tokenizers import df_to_tokenized_sentences
from build_models.build_models import build_bow
from build_models.build_models import get_features_bow
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from tests import log_all_classifier_tests
from tests import data_vs_size_classifiers
from build_classifiers.Logger import graph_vs_datasize
import pandas as pd

train_df = tsv_to_dataframe('./datasets/facebook_part1.tsv')
train_sentences = df_to_tokenized_sentences(train_df)

unsup_train_df = tsv_to_dataframe('./datasets/fb_large.tsv')
unsup_train_sentences = df_to_tokenized_sentences(unsup_train_df)

combined_train_df = pd.concat([train_df,unsup_train_df],ignore_index=True)
combined_train_sentences = df_to_tokenized_sentences(combined_train_df)

bow = build_bow(combined_train_sentences)

train_features = get_features_bow(bow, train_sentences)
train_num = len(train_df)

test_df = tsv_to_dataframe('./datasets/facebook_part2.tsv')
test_sentences = df_to_tokenized_sentences(test_df)
test_features = get_features_bow(bow, test_sentences)
test_num = len(test_df)

maxent = LogisticRegression()
graph_vs_datasize('(large unsup) bow_graphs',maxent,10,'accuracy',
                train_features,train_df["Sentiment"][0:train_num],
                test_features,test_df["Sentiment"][0:test_num])

"""
f = open('./logs/bow_acc_vs_ds.txt','w')

log_all_classifier_tests('./logs/bag_of_words_tokenized.txt',
                        './datasets/facebook_part1.tsv',
                        './datasets/facebook_part2.tsv',
                        train_features,train_df["Sentiment"][0:train_num],
                        test_features,test_df["Sentiment"][0:test_num])

data_vs_size_classifiers(f,
                        './datasets/facebook_part1.tsv',
                        './datasets/facebook_part2.tsv',
                        train_features,train_df["Sentiment"][0:train_num],
                        test_features,test_df["Sentiment"][0:test_num])

f.close()
"""

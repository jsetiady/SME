from gensim.models.keyedvectors import KeyedVectors
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.metrics import f1_score
from master import vectorize_sentences
from preprocessing.Readers import tsvs_to_df
from preprocessing.Tokenizers import bigram_df, process_df
from bayes_opt import BayesianOptimization
from skopt import BayesSearchCV

model = KeyedVectors.load('./models/lstm.bin')

sup_files = ['./datasets/facebook_part1.tsv','./datasets/facebook_part2.tsv']
sup_df = bigram_df(process_df(tsvs_to_df(sup_files)))
vectors = vectorize_sentences(sup_df,model)
# vectors = vectors.reshape((len(vectors),300))
labels = sup_df["Sentiment"].as_array()
# labels = labels.reshape((len(labels),1))

def maxent_cv(C):
    val = cross_val_score(LogisticRegression(C),
                           vectors, labels, 
                          scoring=f1_score, cv=2).mean()
    return val

space = {'C':[0.1,1,10]}

bs = BayesSearchCV(estimator=LogisticRegression(), 
                   #scoring='f1_samples',
                   search_spaces = space)

"""
c_vals = [0.1,1,10]

for c_val in c_vals:
    print "c_val: "+str(c_val)+"; cv: "+str(maxent_cv(c_val))


gp_params = {"alpha":1e-5}

maxent_bo = BayesianOptimization(maxent_cv, 
                                 {'C': (0.001,100)})
maxent_bo.explore({'C': [0.001, 0.01, 0.01]})
maxent_bo.maximize(n_iter=10, **gp_params)

print('maxent: %f' % maxent_bo.res['max']['max_val'])
"""


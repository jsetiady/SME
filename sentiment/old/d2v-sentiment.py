# gensim modules
from gensim import utils
from gensim.models.doc2vec import TaggedDocument
from gensim.models import Doc2Vec

# random shuffle
from random import shuffle

# numpy
import numpy

# classifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

import logging
import sys

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)

class TaggedLineSentence(object):
    def __init__(self, sources):
        self.sources = sources

        flipped = {}

        # make sure that keys are unique
        for key, value in sources.items():
            if value not in flipped:
                flipped[value] = [key]
            else:
                raise Exception('Non-unique prefix encountered')

    def __iter__(self):
        for source, prefix in self.sources.items():
            with utils.smart_open(source) as fin:
                for item_no, line in enumerate(fin):
                    yield TaggedDocument(utils.to_unicode(line).split(), [prefix + '_%s' % item_no])

    def to_array(self):
        self.sentences = []
        for source, prefix in self.sources.items():
            with utils.smart_open(source) as fin:
                for item_no, line in enumerate(fin):
                    self.sentences.append(TaggedDocument(utils.to_unicode(line).split(), [prefix + '_%s' % item_no]))
        return self.sentences

    def sentences_perm(self):
        shuffle(self.sentences)
        return self.sentences

log.info('source load')
sources = {'test-neg.txt':'TEST_NEG', 'test-pos.txt':'TEST_POS', 'train-neg.txt':'TRAIN_NEG', 'train-pos.txt':'TRAIN_POS', 'train-unsup.txt':'TRAIN_UNS'}

log.info('TaggedDocument')
sentences = TaggedLineSentence(sources)

log.info('D2V')
model = Doc2Vec(min_count=1, window=10, size=100, sample=1e-4, negative=5, workers=7)
model.build_vocab(sentences.to_array())

import os.path
if not os.path.isfile('imdb.d2v'):
    log.info('Epoch')
    for epoch in range(10):
	    log.info('EPOCH: {}'.format(epoch))
	    model.train(sentences.sentences_perm(),total_examples=model.corpus_count,epochs=model.iter)

    log.info('Model Save')
    model.save('./imdb.d2v')
else:
    log.info('Model Load')
    model = Doc2Vec.load('./imdb.d2v')

log.info('Sentiment')
train_arrays = numpy.zeros((25000, 100))
train_labels = numpy.zeros(25000)

for i in range(12500):
    prefix_train_pos = 'TRAIN_POS_' + str(i)
    prefix_train_neg = 'TRAIN_NEG_' + str(i)
    train_arrays[i] = model.docvecs[prefix_train_pos]
    train_arrays[12500 + i] = model.docvecs[prefix_train_neg]
    train_labels[i] = 1
    train_labels[12500 + i] = 0

print train_labels

test_arrays = numpy.zeros((25000, 100))
test_labels = numpy.zeros(25000)

for i in range(12500):
    prefix_test_pos = 'TEST_POS_' + str(i)
    prefix_test_neg = 'TEST_NEG_' + str(i)
    test_arrays[i] = model.docvecs[prefix_test_pos]
    test_arrays[12500 + i] = model.docvecs[prefix_test_neg]
    test_labels[i] = 1
    test_labels[12500 + i] = 0

log.info('Fitting')
classifier = LogisticRegression()
classifier.fit(train_arrays, train_labels)

forest = RandomForestClassifier(n_estimators=100)
forest = forest.fit(train_arrays, train_labels)

from sklearn.naive_bayes import GaussianNB
naiveb = GaussianNB()
naiveb = naiveb.fit(train_arrays, train_labels)

from sklearn import svm
svecm = svm.SVC()
svecm = svecm.fit(train_arrays, train_labels)

log.info('Calculating cross-validation')
from sklearn.model_selection import cross_val_score
score_cls = cross_val_score(classifier,train_arrays,train_labels,cv=10)
score_frs = cross_val_score(forest,train_arrays,train_labels,cv=10)
score_nvb = cross_val_score(naiveb,train_arrays,train_labels,cv=10)
score_svm = cross_val_score(svecm,train_arrays,train_labels,cv=10)

print ("Logistic Accuracy: %0.2f (+/- %0.2f)" % (score_cls.mean(), score_cls.std()*2))
print ("Forest Accuracy: %0.2f (+/- %0.2f)" % (score_frs.mean(), score_frs.std()*2))
print ("Naive Bayes Accuracy: %0.2f (+/- %0.2f)" % (score_nvb.mean(), score_nvb.std()*2))
print ("SVM Accuracy: %0.2f (+/- %0.2f)" % (score_svm.mean(), score_nvb.std()*2))

print classifier.score(test_arrays, test_labels)
print forest.score(test_arrays, test_labels)
print naiveb.score(test_arrays, test_labels)
print naiveb.score(test_arrays, test_labels)

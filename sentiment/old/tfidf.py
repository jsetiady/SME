import pandas as pd
import numpy as np
import re
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
# from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import preprocessor as p

def review_to_words(raw_review):
    tweet_text = BeautifulSoup(raw_review, "html.parser").get_text()
    
    p.set_options(p.OPT.URL, p.OPT.MENTION)
    clean_tweet_text = p.clean(tweet_text)

    letters_only = re.sub("[^a-zA-Z]", " ", clean_tweet_text)

    words = letters_only.lower().split()

    stops = set(stopwords.words("bahasa.txt"))

    meaningful_words = [w for w in words if not w in stops]

    return " ".join(meaningful_words)

#Pre-proccessing
train = pd.read_csv("facebook_part1.tsv", header=0,
                    delimiter="\t", quoting=3)

num_reviews= train["Text"].size

clean_train_reviews = []

train_num = num_reviews
for i in xrange( 0, train_num  ):
    clean_train_reviews.append( review_to_words(train["Text"][i]))
    if (i+1)%10 == 0:
        print "Review %d of %d\n" % (i+1,train_num)

#Creating Bag of Words
print "Creating the bag of words"

vectorizer = TfidfVectorizer(analyzer = "word",   
                             tokenizer = None,
                             preprocessor = None,
                             stop_words = None,
                             max_features = 500)

train_data_features = vectorizer.fit_transform(clean_train_reviews)

train_data_features = train_data_features.toarray()

vocab = vectorizer.get_feature_names()

dist = np.sum(train_data_features, axis=0)
for tag, count in zip(vocab, dist):
    print count, tag

#Training random forest
print "Training the random forest..."
from sklearn.ensemble import RandomForestClassifier

import os.path
from sklearn.externals import joblib
if not os.path.isfile('tfidf_benchmark.pkl'):
    forest = RandomForestClassifier(n_estimators = 100) 
    forest = forest.fit( train_data_features, train["Sentiment"][0:train_num])
    print "Dumping model to file..."
    joblib.dump(forest, 'tfidf_benchmark.pkl') 
else:
    print "Loading model from file"
    forest = joblib.load('tfidf_benchmark.pkl')

#Cross-validation test
print "Starting cross-validation..."

from sklearn.model_selection import cross_val_score
scores = cross_val_score(forest, train_data_features, train["Sentiment"][0:train_num], cv=10)

print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

print scores

#Pre-processing test data
test = pd.read_csv("facebook_part2.tsv", header=0, delimiter="\t", 
                                      quoting=3 )

clean_test_reviews = [] 

num_test = len(test["Text"])
for i in xrange(0,num_test):
    if( (i+1) % 10 == 0 ):
        print "Review %d of %d\n" % (i+1, num_test)
    clean_review = review_to_words( test["Text"][i] )
    clean_test_reviews.append( clean_review )

#classying the test data
test_data_features = vectorizer.transform(clean_test_reviews)
test_data_features = test_data_features.toarray()

result = forest.predict(test_data_features)
output = pd.DataFrame( data={"ID":test["ID"][0:num_test], "Sentiment":result} )

output.to_csv( "tfidf_Benchmark.csv", index=False, quoting=3 )

test_num = test["Text"].size 

print forest.score(test_data_features,test["Sentiment"][0:test_num])

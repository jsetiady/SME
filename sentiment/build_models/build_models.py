import os.path
from gensim.models.wrappers import FastText
from gensim.models.keyedvectors import KeyedVectors
from gensim.models import Doc2Vec, Phrases
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np

def load_fasttext(file_model, binary_opt=False):
    return KeyedVectors.load_word2vec_format(file_model, binary=binary_opt)

def build_sentence_vector(model, sentence, size):
    vector = np.zeros(size).reshape((1,size))
    count = 0
    for word in sentence:
        try:
            vector += model[word].reshape((1,size)) # * tfidf[word]
            count += 1
        except KeyError:
            # print word
            # create vector for out of vocab word using fasttext 
            # according to fasttext try above already handles that case      
            continue
    if count != 0:
        vector /= count
    return vector

# add sentiment_to_tag, sentence_to_bigram, TaggedLineSentenceTSV for D2V

def build_d2v(tagged_line_sentences, epochs=10, model_size=100):
    model = Doc2Vec(min_count=1, window=10, size=model_size, sample=1e-4, 
                negative=5, workers=7)
    model.build_vocab(sentences.to_array())
    for epoch in range(epochs):
        model.train(sentences.sentences_perm(),
                    total_examples=model.corpus_count,
                    epochs=model.iter)
    return model

def load_or_build_d2v(file_model):
    if not os.path.isfile(file_model):
        model = build_d2v()
    else:
        model = Doc2Vec.load(file_model)
    return model


def build_bow(sentences, max_num_features=500):
    vectorizer = CountVectorizer(analyzer = "word",
                                tokenizer = None,
                                preprocessor = None,
                                stop_words = None,
                                max_features = max_num_features)
    return vectorizer.fit([" ".join(x) for x in sentences])

def get_features_bow(vectorizer, sentences, max_num_features=500):
    features = vectorizer.transform([" ".join(x) for x in sentences])
    return features.toarray()
    
def get_vocab_bow(bow):
    vectorizer = CountVectorizer(analyzer = "word",
                             tokenizer = None,
                             preprocessor = None,
                             stop_words = None,
                             max_features = 500)
    return vectorizer.get_feature_names()



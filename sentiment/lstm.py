from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Tokenizers import process_df, bigram_df
from preprocessing.Readers import tsv_to_dataframe
from keras.models import Sequential
from keras.layers import Dense, Activation

model = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300

def loadData():
    train_df = tsv_to_dataframe('./datasets/facebook_part1.tsv')
    test_df = tsv_to_dataframe('./datasets/facebook_part2.tsv')

    train_sentences = bigram_df(process_df(train_df))
    test_sentences = bigram_df(process_df(test_df))



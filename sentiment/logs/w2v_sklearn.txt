File Train : ./datasets/facebook_part1.tsv
File Test : ./datasets/facebook_part2.tsv
Maximum Entropy 10-fold Accuracy: 0.57 (+/- 0.08)
Maximum Entropy Accuracy: 0.59
Maximum Entropy F1 score: 0.56
Maximum Entropy Recall: 0.59
Maximum Entropy Precision: 0.56
Maximum Entropy clasification report:
             precision    recall  f1-score   support

         -1       0.52      0.78      0.62       383
          0       0.69      0.55      0.61       488
          1       0.00      0.00      0.00        91

avg / total       0.56      0.59      0.56       962

Maximum Entropy confussion matrix:
298 85 0
221 267 0
55 36 0

Random Forest 10-fold Accuracy: 0.60 (+/- 0.06)
Random Forest Accuracy: 0.68
Random Forest F1 score: 0.67
Random Forest Recall: 0.68
Random Forest Precision: 0.68
Random Forest clasification report:
             precision    recall  f1-score   support

         -1       0.63      0.79      0.70       383
          0       0.74      0.67      0.70       488
          1       0.58      0.24      0.34        91

avg / total       0.68      0.68      0.67       962

Random Forest confussion matrix:
302 79 2
148 326 14
33 36 22

Naive Bayes 10-fold Accuracy: 0.46 (+/- 0.07)
Naive Bayes Accuracy: 0.46
Naive Bayes F1 score: 0.39
Naive Bayes Recall: 0.46
Naive Bayes Precision: 0.58
Naive Bayes clasification report:
             precision    recall  f1-score   support

         -1       0.45      0.91      0.60       383
          0       0.76      0.17      0.28       488
          1       0.11      0.09      0.10        91

avg / total       0.58      0.46      0.39       962

Naive Bayes confussion matrix:
348 19 16
355 84 49
76 7 8

SVM 10-fold Accuracy: 0.44 (+/- 0.04)
SVM Accuracy: 0.50
SVM F1 score: 0.45
SVM Recall: 0.50
SVM Precision: 0.45
SVM clasification report:
             precision    recall  f1-score   support

         -1       0.46      0.26      0.33       383
          0       0.52      0.79      0.62       488
          1       0.00      0.00      0.00        91

avg / total       0.45      0.50      0.45       962

SVM confussion matrix:
99 284 0
102 386 0
13 78 0

MLP 10-fold Accuracy: 0.59 (+/- 0.07)
MLP Accuracy: 0.61
MLP F1 score: 0.58
MLP Recall: 0.61
MLP Precision: 0.57
MLP clasification report:
             precision    recall  f1-score   support

         -1       0.55      0.77      0.64       383
          0       0.70      0.60      0.65       488
          1       0.00      0.00      0.00        91

avg / total       0.57      0.61      0.58       962

MLP confussion matrix:
294 89 0
194 294 0
51 40 0

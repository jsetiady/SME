import sys
import MySQLdb
import csv
#import mysql.connector
#from mysql.connector import errorcode

def getconn(user, passwd, host, port, db):
    conn = MySQLdb.connect(
                    user=user,
                    passwd=passwd,
                    host=host,
                    port=port,
                    db=db)    
    return conn

def nullify(L):
    """Convert empty strings in the given list to None."""

    # helper function
    def f(x):
        if(x == ""):
            return None
        else:
            return x
        
    return [f(x) for x in L]

def loadcsv(cursor, table, filename):

    """
    Open a csv file and load it into a sql table.
    Assumptions:
     - the first line in the file is a header
    """

    f = csv.reader(open(filename))
    
    header = f.next()
    numfields = len(header)

    query = buildInsertCmd(table, numfields)

    for line in f:
        vals = nullify(line)
        cursor.execute(query, vals)

    return

def buildInsertCmd(table, numfields):

    """
    Create a query string with the given table name and the right
    number of format placeholders.
    example:
    >>> buildInsertCmd("foo", 3)
    'insert into foo values (%s, %s, %s)' 
    """
    assert(numfields > 0)
    placeholders = (numfields-1) * "%s, " + "%s"
    query = ("insert into %s" % table) + (" values (%s)" % placeholders)
    return query


try:
    conn = getconn('root', 'root', '192.168.65.62', 8888, 'social_media_emotion')
except MySQLdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)
    
cursor = conn.cursor()

loadcsv(cursor, 'tweets', 'data.csv')

cursor.close()
conn.close()



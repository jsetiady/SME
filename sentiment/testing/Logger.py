import plotly
import plotly.plotly as py
import plotly.graph_objs as go
plotly.tools.set_credentials_file(username='rheza', api_key='6RMGKMc8L2StK1xUCEPv')
import numpy
from sklearn.naive_bayes import GaussianNB
from sklearn import svm
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import precision_score, \
         recall_score, confusion_matrix, classification_report, \
         accuracy_score, f1_score
from sklearn.model_selection import cross_val_score

def logger(classifier, fold, x_train, y_train,
                             x_test, y_test):

    cv_score = cross_val_score(classifier, x_train, y_train, cv=fold)
    prediction = classifier.predict(x_test)
    accuracy = accuracy_score(y_test, prediction)
    f1score = f1_score(y_test, prediction, average='weighted')
    recall = recall_score(y_test, prediction, average='weighted')
    precision = precision_score(y_test, prediction, average='weighted')
    class_report = classification_report(y_test, prediction)
    confusion = confusion_matrix(y_test, prediction)

    return prediction, (cv_score, fold, accuracy, f1score, recall, precision, 
                        class_report, confusion)

def log_prediction(y_test, prediction):
    accuracy = accuracy_score(y_test, prediction)
    f1score = f1_score(y_test, prediction, average='weighted')
    recall = recall_score(y_test, prediction, average='weighted')
    precision = precision_score(y_test, prediction, average='weighted')
    class_report = classification_report(y_test, prediction)
    confusion = confusion_matrix(y_test, prediction)

    return (accuracy, f1score, recall, precision, 
            class_report, confusion)

def log_to_textfile(f,classifier_name, 
                        (cv_score, cv_fold, 
                        accuracy, f1score, recall, precision, 
                        class_report, confusion)):

    f.write('\n' + classifier_name + ' ' + str(cv_fold) 
                 + "-fold Accuracy: %0.2f (+/- %0.2f)" 
                    % (cv_score.mean(), cv_score.std()*2))
    
    f.write('\n' + classifier_name 
                 + ' Accuracy: %0.2f' % accuracy)
    f.write('\n' + classifier_name 
                 + ' F1 score: %0.2f' % f1score)
    f.write('\n' + classifier_name 
                 + ' Recall: %0.2f' % recall)
    f.write('\n' + classifier_name 
                 + ' Precision: %0.2f' % precision)
    f.write('\n' + classifier_name 
                 + ' clasification report:\n') 
    f.write(class_report)
    f.write('\n' + classifier_name + ' confussion matrix:\n')
    numpy.savetxt(f, confusion, fmt='%d')

def graph(f,x_data,y_data):
    # Create a trace
    trace = go.Scatter(
        x = x_data,
        y = y_data,
        mode = 'lines+markers'
    )
    data = [trace]
    # Layout
    layout = go.Layout(
        title = 'Accuracy vs. Data Size',
        yaxis=dict(
            title = 'Accuracy'
        ),
        xaxis=dict(
            title = 'Training data size'
        )
    )
    # Plot and embed in ipython notebook!
    fig = go.Figure(data=data,layout=layout)
    py.plot(fig, filename=f)

def graph_vs_datasize(f,classifier,ds_increments,y_metric,x_train,y_train,
                     x_test,y_test):
    metric_dict = {'cv_scores':0, 'accuracy':2, 'f1':3, 'recall':4, 
                    'precision':5, 'report': 6, 'confusion': 7}
    tot_train = len(x_train)
    x_data = []
    y_data = []
    for i in xrange(1,ds_increments+1):
        curr = i * (tot_train / ds_increments)
        x_data.append(curr)
        classifier.fit(x_train[0:curr],y_train[0:curr])
        test_results = logger(classifier,10,x_train,y_train,x_test,y_test)
        if metric_dict[y_metric] == 0:
            y = 0
            for j in xrange(10):
                y += test_results[1][0][j]
            y /= 10
            y_data.append(y)
        else:
            y = test_results[1][metric_dict[y_metric]]
            y_data.append(y)
    graph(f,x_data,y_data)


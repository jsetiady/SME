from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Tokenizers import process_df, bigram_df
from preprocessing.Readers import tsv_to_dataframe, rpw_to_df
from keras.models import Sequential
from keras.layers import *
import matplotlib.pyplot as plt
from keras.optimizers import *
import numpy as np
import pandas as pd
from Attention import *
from crowdcrafting import tasks_to_df, labeled_only_df
import math
from keras.callbacks import *

timesteps = 37
dimensions = 300
batch_size = 80
epochs = 40
model = 0

w2vmodel = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300
crowd_df = labeled_only_df(tasks_to_df('./datasets/crowdcrafting/tasks_1.json',
                            './datasets/crowdcrafting/runs_1.json'))
train_df = pd.concat([ tsv_to_dataframe('./datasets/facebook_part1.tsv'), rpw_to_df('./datasets/fb4.tsv')],
                     ignore_index=True)
train_df = pd.concat([train_df, tsv_to_dataframe('./datasets/facebook_part2.tsv')])
train_df = labeled_only_df(train_df).reset_index()
train_sentences = bigram_df(process_df(train_df))
train_sentences = train_sentences.sample(frac=1)

def training():
    #timesteps1=-1
    #if(avg_timestep):
        #length_sums = 0
        #for i in range(lentrain_sentences)
            

    #if (shuffle = True):
        #train_sentences = train_sentences.sample(frac=1)
    
    tidak_typos=['ga','gak','gk']
    kalau_typos=['klo','kalau']
    jadi_typos=['jd']
    
    for i in xrange(len(train_sentences['Text'])):
        for j in xrange(len(train_sentences['Text'][i])):
            if train_sentences['Text'][i][j] in tidak_typos:
                print 'sentence no :',i
                print train_sentences['Text'][i][j]
                train_sentences['Text'][i][j]='gak'
                print 'changed! to.......'
                print train_sentences['Text'][i][j]

                continue
            if train_sentences['Text'][i][j] in kalau_typos:
                print 'sentence no :',i
                print 'klo changed to kalo'
                train_sentences['Text'][i][j]='kalo'
                continue
            if train_sentences['Text'][i][j] in jadi_typos:
                print 'sentence no :',i
                print 'jadi typo'
                train_sentences['Text'][i][j]='jadi'
                continue

    m = len(train_sentences)
    global x_train
    global y_train

    np.set_printoptions(threshold='nan')

    x_train = np.empty([m,timesteps,dimensions])
    y_train = np.empty([m,3])

    for i in range(m):
        b = train_sentences["Text"][i]
        for j in range(min(len(b),timesteps)):
            try:
                x_train[i][j] = embedding_model[train_sentences["Text"][i][j]]
            except KeyError:
                x_train[i][j] = np.zeros((300))
                continue
        for k in range(min(len(train_sentences["Text"][i]),timesteps-1),timesteps):
            x_train[i][k] = np.zeros((300))
        y_train[i] = [0,0,0]
        y_train[i][train_sentences["Sentiment"][i]+1]=1

    TRAIN_EXAMPLES=int(math.floor(m*(split_fraction)))
    x_test=x_train[TRAIN_EXAMPLES:]
    y_test=y_train[TRAIN_EXAMPLES:]
    x_train=x_train[:TRAIN_EXAMPLES]
    y_train=y_train[:TRAIN_EXAMPLES]

    model = Sequential()
    model.add(Masking(mask_value=0, input_shape=(timesteps, dimensions)))
    model.add(Bidirectional(LSTM( dimensions,  input_shape=(timesteps, dimensions),
                                return_sequences=True
                                , kernel_regularizer=regularizers.l1_l2(0.01)
                                , recurrent_dropout=0.03
                                , dropout=0.5
                                )))
    model.add(Bidirectional(LSTM( dimensions,  input_shape=(timesteps, dimensions), return_sequences=True
                                , kernel_regularizer=regularizers.l1_l2(0.01)
                                , recurrent_dropout=0.03
                                , dropout=0.5
                                )))
    model.add(Attention())
    model.add(Dense(3, activation='softmax'))
    model.compile(loss="categorical_crossentropy",
                    optimizer=Adagrad(lr=0.01, clipvalue=0.1, epsilon=1e-9),
                    metrics=['accuracy'])
    print(model.summary())
    h = model.fit(x_train, y_train, epochs=epochs,  batch_size=batch_size,
                verbose = 1, shuffle=True, validation_data=(x_test,y_test)
                , callbacks=[EarlyStopping(monitor='val_loss',
                                          patience=2, verbose=0, mode='auto')])

    np.set_printoptions(threshold='nan')

    print "model output vs actual output"
    print np.hstack((model.predict(x_test),y_test))
    print(h.history.keys())

    plt.plot(h.history['acc'])
    plt.plot(h.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

def testpredict(test):
    testW2v = np.zeros((1,timesteps,dimensions))
    timestep=0
    for word in test.split(' '):
        wordvec =w2vmodel[word]
        try:
            testW2v[0][timestep-1]=wordvec
        except KeyError:
            continue
        timestep+=1
    print model.predict(testW2v)
